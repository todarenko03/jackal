﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jackal
{
    public partial class Form3 : Form
    {
        int countOfPlayers;

        Dictionary<int, int> typeOfPlayer = new Dictionary<int, int>()
        {
            { 0,  -1},
            { 1,  -1},
            { 2,  -1},
            { 3,  -1}
        };

        public Form3(int countOfPlayers)
        {
            this.countOfPlayers = countOfPlayers;
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            // Выводится то количество окон, сколько выбрано игроков
            groupBox1.Visible = true;
            groupBox2.Visible = true;

            if (countOfPlayers > 2)
            {
                groupBox3.Visible = true;
            }

            if (countOfPlayers > 3)
            {
                groupBox4.Visible = true;
            }
        }
        // следующие 4 функции: обработка выбора в пользу бота или человека за i-го игрока
        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                typeOfPlayer[0] = 2;
            }

            if (radioButton2.Checked == true)
            {
                typeOfPlayer[0] = 1;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (radioButton3.Checked == true)
            {
                typeOfPlayer[1] = 2;
            }

            if (radioButton4.Checked == true)
            {
                typeOfPlayer[1] = 1;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (radioButton5.Checked == true)
            {
                typeOfPlayer[2] = 2;
            }

            if (radioButton6.Checked == true)
            {
                typeOfPlayer[2] = 1;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (radioButton7.Checked == true)
            {
                typeOfPlayer[3] = 2;
            }

            if (radioButton8.Checked == true)
            {
                typeOfPlayer[3] = 1;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            bool isAllChosen = true;
            // проверка на то, во всех ли формах сделан выбор в пользу того, играет бот или человек
            for (int i = 0; i < countOfPlayers; i++)
            {
                if (typeOfPlayer[i] == -1)
                {
                    isAllChosen = false;
                }
            }
            if (isAllChosen)
            {
                Form4 form4 = new Form4(countOfPlayers, typeOfPlayer);
                form4.Show();
                this.Hide();
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Form3_Closed(object sender, EventArgs e)
        {
            // Завершение работы приложения при закрытии окна
            Application.Exit();
        }
    }
}
