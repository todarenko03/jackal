﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jackal
{
    internal class Tile
    {
        public Image image;
        public TileType type;
        public bool opened;
        public int coins;
        public Position pos;
        public int sizeOfCell;
        public Player team;
        public bool Pirate1;
        public bool Pirate2;
        public bool Pirate3;

        public Tile(TileType type, int sizeOfCell, int x = 0, int y = 0, bool opened = false, int coins = 0, Player team = Player.none, bool pirate1 = false, bool pirate2 = false, bool pirate3 = false)
        {
            this.type = type;
            this.sizeOfCell = sizeOfCell;
            this.opened = opened;
            this.coins = coins;
            this.pos = new Position(x, y);
            this.team = team;
            this.Pirate1 = pirate1;
            this.Pirate2 = pirate2;
            this.Pirate3 = pirate3;

            switch (this.type)
            {
                case (TileType.grass1):
                    image = Image.FromFile("..\\..\\images\\empty-1.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.grass2):
                    image = Image.FromFile("..\\..\\images\\empty-2.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.grass3):
                    image = Image.FromFile("..\\..\\images\\empty-3.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.grass4):
                    image = Image.FromFile("..\\..\\images\\empty-4.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.rum):
                    image = Image.FromFile("..\\..\\images\\keg-of-rum.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.ice):
                    image = Image.FromFile("..\\..\\images\\ice.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.hole):
                    image = Image.FromFile("..\\..\\images\\pitfall.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.croc):
                    image = Image.FromFile("..\\..\\images\\crocodile.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.cannibal):
                    image = Image.FromFile("..\\..\\images\\cannibal.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.fort):
                    image = Image.FromFile("..\\..\\images\\fort.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.gfort):
                    image = Image.FromFile("..\\..\\images\\fort-w-aborigine.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.coins1):
                    image = Image.FromFile("..\\..\\images\\coins-1.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    this.coins = 1;
                    break;

                case (TileType.coins2):
                    image = Image.FromFile("..\\..\\images\\coins-2.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    this.coins = 2;
                    break;

                case (TileType.coins3):
                    image = Image.FromFile("..\\..\\images\\coins-3.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    this.coins = 3;
                    break;

                case (TileType.coins4):
                    image = Image.FromFile("..\\..\\images\\coins-4.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    this.coins = 4;
                    break;

                case (TileType.coins5):
                    image = Image.FromFile("..\\..\\images\\coins-5.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    this.coins = 5;
                    break;

                case (TileType.water):
                    image = Image.FromFile("..\\..\\images\\water.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.balloon):
                    image = Image.FromFile("..\\..\\images\\balloon.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.astr1):
                    image = Image.FromFile("..\\..\\images\\arrow-1.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.adiag1):
                    image = Image.FromFile("..\\..\\images\\arrow-2.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.adiag2):
                    image = Image.FromFile("..\\..\\images\\arrow-4.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.astr2):
                    image = Image.FromFile("..\\..\\images\\arrow-3.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.astr4):
                    image = Image.FromFile("..\\..\\images\\arrow-5.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.adiag4):
                    image = Image.FromFile("..\\..\\images\\arrow-6.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.lab2):
                    image = Image.FromFile("..\\..\\images\\rotate-2.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.lab3):
                    image = Image.FromFile("..\\..\\images\\rotate-3.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.lab4):
                    image = Image.FromFile("..\\..\\images\\rotate-4.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.lab5):
                    image = Image.FromFile("..\\..\\images\\rotate-5.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;


                case (TileType.ship1):
                    image = Image.FromFile("..\\..\\images\\ship-1.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.ship2):
                    image = Image.FromFile("..\\..\\images\\ship-2.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.ship3):
                    image = Image.FromFile("..\\..\\images\\ship-3.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                case (TileType.ship4):
                    image = Image.FromFile("..\\..\\images\\ship-4.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;

                default:
                    image = Image.FromFile("..\\..\\images\\empty.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                    break;
            }

            if (!opened)
            {
                image = Image.FromFile("..\\..\\images\\closed.png");
                image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
            }

            if (team == Player.white)
            {
                if (Pirate1 || Pirate2 || Pirate3)
                {
                    image = Image.FromFile("..\\..\\images\\pirate1.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                }
            }

            if (team == Player.black)
            {
                if (Pirate1 || Pirate2 || Pirate3)
                {
                    image = Image.FromFile("..\\..\\images\\pirate2.jpg");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                }
            }

            if (team == Player.red)
            {
                if (Pirate1 || Pirate2 || Pirate3)
                {
                    image = Image.FromFile("..\\..\\images\\pirate3.png");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                }
            }

            if (team == Player.yellow)
            {
                if (Pirate1 || Pirate2 || Pirate3)
                {
                    image = Image.FromFile("..\\..\\images\\pirate4.jpg");
                    image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                }
            }

        }

        public void Open(bool removePirates = false)
        {
            opened = true;

            if (removePirates)
            {
                Pirate1 = false;
                Pirate2 = false;
                Pirate3 = false;
                team = Player.none;
            }

            if (!Pirate1 & !Pirate2 & !Pirate3)
            {
                switch (type)
                {
                    case (TileType.grass1):
                        image = Image.FromFile("..\\..\\images\\empty-1.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.grass2):
                        image = Image.FromFile("..\\..\\images\\empty-2.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.grass3):
                        image = Image.FromFile("..\\..\\images\\empty-3.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.grass4):
                        image = Image.FromFile("..\\..\\images\\empty-4.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.rum):
                        image = Image.FromFile("..\\..\\images\\keg-of-rum.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.ice):
                        image = Image.FromFile("..\\..\\images\\ice.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.hole):
                        image = Image.FromFile("..\\..\\images\\pitfall.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.croc):
                        image = Image.FromFile("..\\..\\images\\crocodile.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.cannibal):
                        image = Image.FromFile("..\\..\\images\\cannibal.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.fort):
                        image = Image.FromFile("..\\..\\images\\fort.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.gfort):
                        image = Image.FromFile("..\\..\\images\\fort-w-aborigine.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.coins1):
                        image = Image.FromFile("..\\..\\images\\coins-1.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.coins2):
                        image = Image.FromFile("..\\..\\images\\coins-2.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.coins3):
                        image = Image.FromFile("..\\..\\images\\coins-3.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.coins4):
                        image = Image.FromFile("..\\..\\images\\coins-4.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.coins5):
                        image = Image.FromFile("..\\..\\images\\coins-5.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.water):
                        image = Image.FromFile("..\\..\\images\\water.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.balloon):
                        image = Image.FromFile("..\\..\\images\\balloon.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.astr1):
                        image = Image.FromFile("..\\..\\images\\arrow-1.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.adiag1):
                        image = Image.FromFile("..\\..\\images\\arrow-2.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.adiag2):
                        image = Image.FromFile("..\\..\\images\\arrow-4.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.astr2):
                        image = Image.FromFile("..\\..\\images\\arrow-3.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;


                    case (TileType.astr4):
                        image = Image.FromFile("..\\..\\images\\arrow-5.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.adiag4):
                        image = Image.FromFile("..\\..\\images\\arrow-6.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.lab2):
                        image = Image.FromFile("..\\..\\images\\rotate-2.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.lab3):
                        image = Image.FromFile("..\\..\\images\\rotate-3.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.lab4):
                        image = Image.FromFile("..\\..\\images\\rotate-4.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.lab5):
                        image = Image.FromFile("..\\..\\images\\rotate-5.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.ship1):
                        image = Image.FromFile("..\\..\\images\\ship-1.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.ship2):
                        image = Image.FromFile("..\\..\\images\\ship-2.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.ship3):
                        image = Image.FromFile("..\\..\\images\\ship-3.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    case (TileType.ship4):
                        image = Image.FromFile("..\\..\\images\\ship-4.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;

                    default:
                        image = Image.FromFile("..\\..\\images\\empty.png");
                        image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                        break;
                }
            }
        }

        public void putPirateOnTile(Player team)
        {
            if (team == Player.white)
            {
                image = Image.FromFile("..\\..\\images\\pirate1.png");
                image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
            }
            else if (team == Player.black)
            {
                image = Image.FromFile("..\\..\\images\\pirate2.jpg");
                image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
            }
            else if (team == Player.red)
            {
                image = Image.FromFile("..\\..\\images\\pirate3.png");
                image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
            }
            else if (team == Player.yellow)
            {
                image = Image.FromFile("..\\..\\images\\pirate4.jpg");
                image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
            }
        }

        public void SetShip(int number)
        {
            Pirate1 = false;
            Pirate2 = false;
            Pirate3 = false;
            if (number == 0)
            {
                image = Image.FromFile("..\\..\\images\\ship-1.png");
                image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                type = TileType.ship1;
            }

            else if (number == 1)
            {
                image = Image.FromFile("..\\..\\images\\ship-2.png");
                image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                type = TileType.ship2;
            }

            else if (number == 2)
            {
                image = Image.FromFile("..\\..\\images\\ship-3.png");
                image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                type = TileType.ship3;
            }

            else if (number == 3)
            {
                image = Image.FromFile("..\\..\\images\\ship-4.png");
                image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
                type = TileType.ship4;
            }
        }

        public void SetWater()
        {
            image = Image.FromFile("..\\..\\images\\water.png");
            image = new Bitmap(image, new Size(sizeOfCell, sizeOfCell));
            type = TileType.water;
        }
    }
}
