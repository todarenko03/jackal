﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jackal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.button1.Location = new Point(this.Width / 2 - this.button1.Width / 2, this.Height / 2 - this.button1.Height * 2 - 3);
            this.button2.Location = new Point(this.Width / 2 - this.button2.Width / 2, this.Height / 2 - this.button1.Height + 3);
            // Кнопки располагаются в зависимости от размера окна
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            this.button1.Location = new Point(this.Width / 2 - this.button1.Width / 2, this.Height / 2 - this.button1.Height * 2 - 3);
            this.button2.Location = new Point(this.Width / 2 - this.button2.Width / 2, this.Height / 2 - this.button1.Height + 3);
            // Кнопки сохраняют относительное положение после изменения размера окна
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
            this.Hide();
            // Создается новая форма, старая скрывается
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
