﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jackal
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int countOfPlayers = 0;

            //Выбор количества игроков

            if (radioButton1.Checked == true)
            {
                countOfPlayers = 2;
            }

            else if (radioButton2.Checked == true)
            {
                countOfPlayers = 3;
            }

            else if (radioButton3.Checked == true)
            {
                countOfPlayers = 4;
            }

            // Создается новое окно, текущее скрывается 

            var form3 = new Form3(countOfPlayers);
            form3.Show();
            this.Hide();
        }

        private void Form2_Closed(object sender, EventArgs e)
        {
            // Завершение работы приложения при закрытии окна
            Application.Exit();
        }
    }
}
