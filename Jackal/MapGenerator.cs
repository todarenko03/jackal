﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jackal
{
    internal class MapGenerator
    {
        public static int sizeOfField = 13;
        public static int sizeOfCell = 58;
        public Button[,] buttons = new Button[sizeOfField, sizeOfField];
        public Tile[,] tiles = new Tile[sizeOfField, sizeOfField];
        public PictureBox[,] pirates = new PictureBox[4, 3];
        public static Random rand = new Random();
        int countOfPlayers;
        Form4 form;
        public MapGenerator(int countOfPlayers, Form4 form)
        {
            List<Tile> generatedTiles = GenerateAllTiles();
            this.countOfPlayers = countOfPlayers;
            this.form = form;

            //Создание ячеек поля, которыми являются кнопки

            for (int i = 0; i < sizeOfField; i++)
            {
                for (int j = 0; j < sizeOfField; j++)
                {
                    var button = new Button();
                    button.Location = new System.Drawing.Point(sizeOfCell * j, sizeOfCell * i);
                    button.Size = new System.Drawing.Size(sizeOfCell, sizeOfCell);
                    form.Controls.Add(button);
                    buttons[i, j] = button;
                }
            }

            // следующие 4 цикла устанавливают границы(крайние элементы поля) в виде воды

            for (int i = 0; i < sizeOfField; i++)
            {
                SetWater(0, i, tiles);
                buttons[0, i].BackgroundImage = tiles[0, i].image;
            }

            for (int i = 0; i < sizeOfField; i++)
            {
                SetWater(i, 0, tiles);
                buttons[i, 0].BackgroundImage = tiles[i, 0].image;
            }

            for (int i = 0; i < sizeOfField; i++)
            {
                SetWater(i, sizeOfField - 1, tiles);
                buttons[i, sizeOfField - 1].BackgroundImage = tiles[i, sizeOfField - 1].image;
            }

            for (int i = 0; i < sizeOfField; i++)
            {
                SetWater(sizeOfField - 1, i, tiles);
                buttons[sizeOfField - 1, i].BackgroundImage = tiles[sizeOfField - 1, i].image;
            }

            // установка случайного тайла в каждую позицию за исключением границы
            for (int i = 1; i < sizeOfField - 1; i++)
            {
                for (int j = 1; j < sizeOfField - 1; j++)
                {
                    SetRandomTile(i, j, tiles, generatedTiles);
                    buttons[i, j].BackgroundImage = tiles[i, j].image;
                }
            }

            //Дополнительная установка тайлов воды по углам (дополнительная граница)
            SetWater(1, 1, tiles);
            buttons[1, 1].BackgroundImage = tiles[1, 1].image;
            SetWater(sizeOfField - 2, 1, tiles);
            buttons[sizeOfField - 2, 1].BackgroundImage = tiles[sizeOfField - 2, 1].image;
            SetWater(1, sizeOfField - 2, tiles);
            buttons[1, sizeOfField - 2].BackgroundImage = tiles[1, sizeOfField - 2].image;
            SetWater(sizeOfField - 2, sizeOfField - 2, tiles);
            buttons[sizeOfField - 2, sizeOfField - 2].BackgroundImage = tiles[sizeOfField - 2, sizeOfField - 2].image;

            // Установка тайлов корабля
            if (countOfPlayers > 1)
            {
                SetShip(0, sizeOfField / 2, tiles, 1, player: Player.white);
                buttons[0, sizeOfField / 2].BackgroundImage = tiles[0, sizeOfField / 2].image;
                SetShip(sizeOfField - 1, sizeOfField / 2, tiles, 2, player: Player.black);
                buttons[sizeOfField - 1, sizeOfField / 2].BackgroundImage = tiles[sizeOfField - 1, sizeOfField / 2].image;
            }

            if (countOfPlayers > 2)
            {
                SetShip(sizeOfField / 2, 0, tiles, 3, player: Player.red);
                buttons[sizeOfField / 2, 0].BackgroundImage = tiles[sizeOfField / 2, 0].image;
            }

            if (countOfPlayers > 3)
            {
                SetShip(sizeOfField / 2, sizeOfField - 1, tiles, 4, player: Player.yellow);
                buttons[sizeOfField / 2, sizeOfField - 1].BackgroundImage = tiles[sizeOfField / 2, sizeOfField - 1].image;
            }
        }
    
        //установка тайла воды
        public void SetWater(int x, int y, Tile[,] board)
        {
            board[x, y] = new Tile(TileType.water, sizeOfCell, x, y, true);
        }

        //установка случайного тайла
        public void SetRandomTile(int x, int y, Tile[,] board, List<Tile> generatedTiles)
        {   if (generatedTiles.Count == 0)
            {
                board[x, y] = new Tile(TileType.grass1, sizeOfCell);
            }
            else
            {
                int r = rand.Next() % generatedTiles.Count;
                board[x, y] = generatedTiles[r];
                generatedTiles.RemoveAt(r);
            }
        }

        //функция установки тайла корабля
        public void SetShip(int x, int y, Tile[,] board, int numOfTeam, Player player = Player.none)
        {
            if (numOfTeam == 1)
            {
                board[x, y] = new Tile(TileType.ship1, sizeOfCell, opened: true, team: player, pirate1: true, pirate2: true, pirate3: true);
            }

            else if (numOfTeam == 2)
            {
                board[x, y] = new Tile(TileType.ship2, sizeOfCell, opened: true, team: player, pirate1: true, pirate2: true, pirate3: true);
            }

            else if (numOfTeam == 3)
            {
                board[x, y] = new Tile(TileType.ship3, sizeOfCell, opened: true, team: player, pirate1: true, pirate2: true, pirate3: true);
            }

            else if (numOfTeam == 4)
            {
                board[x, y] = new Tile(TileType.ship4, sizeOfCell, opened: true, team: player, pirate1: true, pirate2: true, pirate3: true);
            }
        }

        //генерация списка с нужным количеством тайлов для игры (количество каждого вида тайлов фиксировано)
        List<Tile> GenerateAllTiles()
        {
            List<Tile> tiles = new List<Tile>();

            for (int i = 0; i < 8; i++)
                tiles.Add(new Tile(TileType.grass2, sizeOfCell, opened: false)); //grass

            for (int g = 1; g < 5; g++)
                for (int i = 0; i < 10; i++)
                    tiles.Add(new Tile((TileType)g, sizeOfCell, opened: false)); //grass

            for (int a = 5; a < 11; a++)
                for (int i = 0; i < 3; i++)
                    tiles.Add(new Tile((TileType)a, sizeOfCell, opened: false)); //arrows

            for (int i = 0; i < 4; i++)
                tiles.Add(new Tile(TileType.rum, sizeOfCell, opened: false));
            for (int i = 0; i < 5; i++)
                tiles.Add(new Tile(TileType.lab2, sizeOfCell, opened: false));
            for (int i = 0; i < 4; i++)
                tiles.Add(new Tile(TileType.lab3, sizeOfCell, opened: false));
            for (int i = 0; i < 2; i++)
                tiles.Add(new Tile(TileType.lab4, sizeOfCell, opened: false));

            tiles.Add(new Tile(TileType.lab5, sizeOfCell, opened: false));

            for (int i = 0; i < 6; i++)
                tiles.Add(new Tile(TileType.ice, sizeOfCell, opened: false));
            for (int i = 0; i < 3; i++)
                tiles.Add(new Tile(TileType.hole, sizeOfCell, opened: false));
            for (int i = 0; i < 4; i++)
                tiles.Add(new Tile(TileType.croc, sizeOfCell, opened: false));
            tiles.Add(new Tile(TileType.cannibal, sizeOfCell, opened: false));

            for (int i = 0; i < 2; i++)
                tiles.Add(new Tile(TileType.fort, sizeOfCell, opened: false));
            tiles.Add(new Tile(TileType.gfort, sizeOfCell, opened: false));

            for (int i = 0; i < 5; i++)
                tiles.Add(new Tile(TileType.coins1, sizeOfCell, opened: false, coins:1));
            for (int i = 0; i < 5; i++)
                tiles.Add(new Tile(TileType.coins2, sizeOfCell, opened: false, coins: 2));
            for (int i = 0; i < 3; i++)
                tiles.Add(new Tile(TileType.coins3, sizeOfCell, opened: false, coins: 3));
            for (int i = 0; i < 2; i++)
                tiles.Add(new Tile(TileType.coins4, sizeOfCell, opened: false, coins: 4));
            tiles.Add(new Tile(TileType.coins5, sizeOfCell, opened: false, coins: 5));

            for (int i = 0; i < 2; i++)
                tiles.Add(new Tile(TileType.balloon, sizeOfCell, opened: false));

            return tiles;
        }
    }
}
