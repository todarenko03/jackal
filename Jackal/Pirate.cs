﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jackal
{
    // Класс пирата
    internal class Pirate
    {
        public int id;
        public Player team;
        public Position position;
        public int gold;
        public bool alive;
        public int drunks;
        public bool selected;
        public bool trapped;

        public Pirate(int id, Player team, int x = 0, int y = 0, bool selected = false, int drunks = 0)
        {
            this.id = id;
            this.team = team;
            this.position = new Position(x, y);
            this.gold = 0;
            this.alive = true;
            this.drunks = drunks;
            this.selected = selected;
            this.trapped = false;
        }
    }
}
