﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jackal
{
    public partial class Form5 : Form
    {
        int white;
        int black;
        int red;
        int yellow;
        int countOfPlayers;
        public Form5(int white, int black, int red, int yellow, int countOfPlayers)
        {
            InitializeComponent();
            this.white = white;
            this.black = black;
            this.red = red;
            this.yellow = yellow;
            this.countOfPlayers = countOfPlayers;
            if ((white > black) & (white > red) & (white > yellow))
            {
                label1.Text = "Победил 1 игрок";
            }
            else if ((black > white) & (black > red) & (black > yellow))
            {
                label1.Text = "Победил 2 игрок";
            }
            else if ((red > white) & (red > black) & (red > yellow))
            {
                label1.Text = "Победил 3 игрок";
            }
            else if ((yellow > white) & (yellow > red) & (yellow > black))
            {
                label1.Text = "Победил 4 игрок";
            }
            else if (white == black & black == red & red == yellow)
            {
                label1.Text = "Победили 1, 2, 3 и 4 игроки";
            }
            else if (white == black & black == red)
            {
                label1.Text = "Победили 1, 2 и 3 игроки";
            }
            else if (white == red & red == yellow)
            {
                label1.Text = "Победили 1, 3 и 4 игроки";
            }
            else if (white == black & black == yellow)
            {
                label1.Text = "Победили 1, 2, и 4 игроки";
            }
            else if (black == red & red == yellow)
            {
                label1.Text = "Победили 2, 3 и 4 игроки";
            }
            else if (white == black)
            {
                label1.Text = "Победили 1, 2 игроки";
            }
            else if (white == red)
            {
                label1.Text = "Победили 1, 3 игроки";
            }
            else if (white == yellow)
            {
                label1.Text = "Победили 1, 4 игроки";
            }
            else if (black == red)
            {
                label1.Text = "Победили  2, 3 игроки";
            }
            else if (black == yellow)
            {
                label1.Text = "Победили 2, 4 игроки";
            }
            else if (red == yellow)
            {
                label1.Text = "Победили 3 и 4 игроки";
            }
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }

        private void Form5_Closed(object sender, EventArgs e)
        {
            // Завершение работы приложения при закрытии окна
            Application.Exit();
        }
    }
}
