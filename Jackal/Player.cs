﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jackal
{
    // Игрок
    internal enum Player
    {
        none,
        black,
        white,
        red,
        yellow
    }
}
