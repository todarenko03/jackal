﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jackal
{
    internal class Game
    {
        int sizeOfCell = 58;
        int sizeOfField = 13;
        int countOfCoins = 37;
        int bot1step = 0;
        int bot2step = 0;
        int bot3step = 0;
        int bot4step = 0;
        Button[,] buttons;
        Tile[,] tiles;
        Pirate[,] pirates;
        Form4 form;
        int countOfPlayers;
        int tempStep;
        int ox;
        int oy;
        bool stop;
        bool pirateWasClicked;
        bool pirate1WasChosen;
        bool pirate2WasChosen;
        bool pirate3WasChosen;
        bool shipWasChosen;
        TileType chosenTile;
        Label message;
        Label[] scoreLabels;
        Label step;
        int[] score;
        Button button1;
        Button button2;
        Button button3;
        Dictionary<int, int> typeOfPlayer;
        static Random rnd = new Random();

        public Game(Button[,] buttons, Tile[,] tiles, int countOfPlayers ,Form4 form, Dictionary<int, int> typeOfPlayer)
        {
            this.buttons = buttons;
            this.tiles = tiles;
            this.countOfPlayers = countOfPlayers;
            this.form = form;
            this.typeOfPlayer = typeOfPlayer;
            tempStep = 0;
            pirateWasClicked = false;
            message = new Label();
            stop = true;
            pirate1WasChosen = false;
            pirate2WasChosen = false;
            pirate3WasChosen = false;
            shipWasChosen = false;
            pirates = new Pirate[countOfPlayers, 3];
            scoreLabels = new Label[countOfPlayers];
            score = new int[4];
            step = new Label();

            button1 = new Button();
            button1.Location = new System.Drawing.Point(sizeOfCell * 14, sizeOfCell);
            button1.Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 5);
            button1.Click += new EventHandler(clickOnPirateButton1);
            form.Controls.Add(button1);
            button2 = new Button();
            button2.Location = new System.Drawing.Point(sizeOfCell * 14, sizeOfCell * 2);
            button2.Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 5);
            button2.Click += new EventHandler(clickOnPirateButton2);
            form.Controls.Add(button2);
            button3 = new Button();
            button3.Location = new System.Drawing.Point(sizeOfCell * 14, sizeOfCell * 3);
            button3.Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 5);
            button3.Click += new EventHandler(clickOnPirateButton3);
            form.Controls.Add(button3);
            var button4 = new Button();
            button4.Location = new System.Drawing.Point(sizeOfCell * 14, sizeOfCell * 4);
            button4.Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 5);
            button4.Click += new EventHandler(clickOnShip);
            form.Controls.Add(button4);
            var button5 = new Button();
            button5.Location = new System.Drawing.Point(sizeOfCell * 14, sizeOfCell * 5);
            button5.Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 5);
            button5.Click += new EventHandler(TakeCoin);
            form.Controls.Add(button5);
            var button6 = new Button();
            button6.Location = new System.Drawing.Point(sizeOfCell * 14, sizeOfCell * 6);
            button6.Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 5);
            button6.Click += new EventHandler(deselect);
            form.Controls.Add(button6);
            var button7 = new Button();
            button7.Location = new System.Drawing.Point(sizeOfCell * 14, sizeOfCell * 7);
            button7.Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 5);
            button7.Click += new EventHandler(nextBotMove);
            form.Controls.Add(button7);

            message.Location = new System.Drawing.Point(sizeOfCell * 14, sizeOfCell * 8);
            message.Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 5);
            form.Controls.Add(message);

            step.Location = new System.Drawing.Point(sizeOfCell * 14, sizeOfCell * 9);
            step.Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 5);
            form.Controls.Add(step);
            step.Text = $"текущий ход: {tempStep + 1}";

            button1.Text = "1 пират";
            button2.Text = "2 пират";
            button3.Text = "3 пират";
            button4.Text = "корабль";
            button5.Text = "взять монету";
            button6.Text = "отмена";
            button7.Text = "ход бота";

            for (int i = 0; i < sizeOfField; i++)
            {
                for (int j = 0; j < sizeOfField; j++)
                {
                    buttons[i, j].Click += new EventHandler(clickOnTile);
                }
            }

            for (int i = 0; i < countOfPlayers; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Player p = Player.none;

                    if (i == 0)
                    {
                        p =  Player.white;
                    }

                    else if (i == 1)
                    {
                        p = Player.black;
                    }

                    else if (i == 2)
                    {
                        p = Player.red;
                    }

                    else if (i == 3)
                    {
                        p = Player.yellow;
                    }
                    pirates[i, j] = new Pirate(j, p);
                }
            }

            for (int i = 0; i < 4; i++)
            {
                score[i] = 0;
            }

            for (int i = 0; i < countOfPlayers; i++)
            {
                scoreLabels[i] = new Label();
                scoreLabels[i].Location = new System.Drawing.Point(sizeOfCell * 14, (sizeOfCell - i) * (10 + i));
                scoreLabels[i].Size = new System.Drawing.Size(sizeOfCell + 5, sizeOfCell - 12);
                scoreLabels[i].Text = $"{i + 1} команда: {score[i]}";
                form.Controls.Add(scoreLabels[i]);
            }
        }

        public void Move(Button button, int ny, int nx, Tile tile)
        {
            if (!pirates[tempStep, 0].alive & !pirates[tempStep, 1].alive & !pirates[tempStep, 2].alive)
            {
                tempStep += 1;
                tempStep %= countOfPlayers;
            }
            else if ((nx != ox || ny != oy) & tile.type != TileType.water)
            {
                if (pirate1WasChosen & Math.Abs(ny - oy) <= 1 & Math.Abs(nx - ox) <= 1 & pirates[tempStep, 0].alive)
                {
                    var repeatMove = 0;
                    var player = tempPlayer();
                    if (tiles[oy, ox].type == TileType.ship1 || tiles[oy, ox].type == TileType.ship2 ||
                             tiles[oy, ox].type == TileType.ship3 || tiles[oy, ox].type == TileType.ship4)
                    {
                        if (tiles[oy, ox].type == TileType.ship1)
                        {
                            if (ny != oy + 1 || nx != ox)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }

                        else if (tiles[oy, ox].type == TileType.ship2)
                        {
                            if (ny != oy - 1 || nx != ox)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }

                        else if (tiles[oy, ox].type == TileType.ship3)
                        {
                            if (ny != oy || nx != ox + 1)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }

                        else if (tiles[oy, ox].type == TileType.ship4)
                        {
                            if (ny != oy || nx != ox - 1)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }
                    }
                    if (tile.type == TileType.ice)
                    {
                        tile.Open();
                        buttons[ny, nx].Image = tiles[ny, nx].image;
                        tiles[oy, ox].Pirate1 = false;
                        var y = ny - oy;
                        var x = nx - ox;
                        oy = ny;
                        ox = nx;
                        ny += y;
                        nx += x;
                        if (tiles[ny, nx].type == TileType.croc)
                        {
                            tiles[ny, nx].Open();
                            buttons[ny, nx].Image = tiles[ny, nx].image;
                            ny = oy - y;
                            nx = ox - x;
                            pirates[tempStep, 0].position = new Position(ny, nx);
                            tiles[ny, nx].Pirate1 = true;
                            tiles[ny, nx].putPirateOnTile(player);
                        }
                        else if (tiles[ny, nx].type == TileType.water)
                        {
                            pirates[tempStep, 0].alive = false;
                            pirates[tempStep, 0].gold = 0;
                            pirates[tempStep, 0].position = new Position(-1, -1);
                        }
                        else if (tiles[ny, nx].type == TileType.ship1 || tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship4)
                        {
                            if (tiles[ny, nx].team == Player.white & (tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship4))
                            {
                                pirates[tempStep, 0].alive = false;
                                pirates[tempStep, 0].gold = 0;
                                pirates[tempStep, 0].position = new Position(-1, -1);
                            }

                            else if (tiles[ny, nx].team == Player.black & (tiles[ny, nx].type == TileType.ship1 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship4))
                            {
                                pirates[tempStep, 0].alive = false;
                                pirates[tempStep, 0].gold = 0;
                                pirates[tempStep, 0].position = new Position(-1, -1);
                            }

                            else if (tiles[ny, nx].team == Player.red & (tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship1 || tiles[ny, nx].type == TileType.ship4))
                            {
                                pirates[tempStep, 0].alive = false;
                                pirates[tempStep, 0].gold = 0;
                                pirates[tempStep, 0].position = new Position(-1, -1);
                            }

                            else if (tiles[ny, nx].team == Player.yellow & (tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship1))
                            {
                                pirates[tempStep, 0].alive = false;
                                pirates[tempStep, 0].gold = 0;
                                pirates[tempStep, 0].position = new Position(-1, -1);
                            }
                        }

                        else
                        {
                            if (!tiles[oy - y, ox - x].Pirate1 & !tiles[oy - y, ox - x].Pirate2 & !tiles[oy - y, ox - x].Pirate3)
                            {
                                tiles[oy - y, ox - x].Open(removePirates: true);
                                buttons[oy - y, ox - x].Image = tiles[oy - y, ox - x].image;
                            }
                            Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                            repeatMove = 1;
                        }
                    }
                    else if (tile.team != Player.none & tile.team != tempPlayer() & pirates[tempStep, 0].gold == 0 & tile.type != TileType.fort)
                    {
                        tiles[oy, ox].Pirate1 = false;
                        if (tile.team == Player.white)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[0, i].type == TileType.ship1)
                                {
                                    y = 0;
                                    x = i;
                                }
                            }
                            tiles[y, x].team = Player.white;
                            tiles[y, x].putPirateOnTile(Player.white);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[0, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[0, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[0, 2].position = new Position(y, x);
                            }
                        }
                        else if (tile.team == Player.black)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[sizeOfField - 1, i].type == TileType.ship2)
                                {
                                    y = sizeOfField - 1;
                                    x = i;
                                }
                            }
                            tiles[y, x].team = Player.black;
                            tiles[y, x].putPirateOnTile(Player.black);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[1, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[1, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[1, 2].position = new Position(y, x);
                            }
                        }
                        else if (tile.team == Player.red)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[i, 0].type == TileType.ship3)
                                {
                                    y = i;
                                    x = 0;
                                }
                            }
                            tiles[y, x].team = Player.red;
                            tiles[y, x].putPirateOnTile(Player.red);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[2, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[2, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[2, 2].position = new Position(y, x);
                            }
                        }
                        else if (tile.team == Player.yellow)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[i, sizeOfField - 1].type == TileType.ship4)
                                {
                                    y = i;
                                    x = sizeOfField - 1;
                                }
                            }
                            tiles[y, x].team = Player.yellow;
                            tiles[y, x].putPirateOnTile(Player.yellow);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[3, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[3, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[3, 2].position = new Position(y, x);
                            }
                        }
                        tile.Pirate1 = true;
                        tile.team = tempPlayer();
                        tile.putPirateOnTile(tempPlayer());
                        pirates[tempStep, 0].position = new Position(ny, nx);
                        button.Image = tile.image;
                    }
                    else if (tile.team == Player.none || tile.team == tempPlayer())
                    {
                        tile.Open();
                        button.Image = tile.image;
                        if (tile.type == TileType.cannibal)
                        {
                            pirates[tempStep, 0].alive = false;
                            pirates[tempStep, 0].gold = 0;
                            pirates[tempStep, 0].position = new Position(-1, -1);
                            tiles[oy, ox].Pirate1 = false;
                            ny = -1;
                            nx = -1;
                        }
                        else if (tile.type == TileType.croc)
                        {
                            ny = oy;
                            nx = ox;
                        }
                        else if (tile.type == TileType.balloon)
                        {
                            if (player == Player.white)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[0, i].type == TileType.ship1)
                                    {
                                        y = 0;
                                        x = i;
                                    }
                                }
                                tiles[oy, ox].Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                            else if (player == Player.black)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[sizeOfField - 1, i].type == TileType.ship2)
                                    {
                                        y = sizeOfField - 1;
                                        x = i;
                                    }
                                }
                                tiles[oy, ox].Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                            else if (player == Player.red)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[i, 0].type == TileType.ship3)
                                    {
                                        y = i;
                                        x = 0;
                                    }
                                }
                                tiles[oy, ox].Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                            else if (player == Player.yellow)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[i, sizeOfField - 1].type == TileType.ship4)
                                    {
                                        y = i;
                                        x = sizeOfField - 1;
                                    }
                                }
                                tiles[oy, ox].Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                        }
                        else if (tile.type == TileType.gfort)
                        {
                            tile.putPirateOnTile(player);
                            tile.team = player;
                            tiles[oy, ox].Pirate1 = false;
                            tile.Pirate1 = true;
                            buttons[ny, nx].Image = tiles[ny, nx].image;
                            if (!pirates[tempStep, 1].alive)
                            {
                                pirates[tempStep, 1].alive = true;
                                pirates[tempStep, 1].position = new Position(ny, nx);
                                tile.Pirate2 = true;
                            }
                            if (!pirates[tempStep, 2].alive)
                            {
                                pirates[tempStep, 2].alive = true;
                                pirates[tempStep, 2].position = new Position(ny, nx);
                                tile.Pirate3 = true;
                            }
                        }
                        else
                        {
                            tile.putPirateOnTile(player);
                            tile.team = player;
                            tiles[oy, ox].Pirate1 = false;
                            tile.Pirate1 = true;
                            buttons[ny, nx].Image = tiles[ny, nx].image;
                        }
                    }
                    if (!tiles[oy, ox].Pirate1 & !tiles[oy, ox].Pirate2 & !tiles[oy, ox].Pirate3)
                    {
                        tiles[oy, ox].Open(removePirates: true);
                        buttons[oy, ox].Image = tiles[oy, ox].image;
                    }

                    if ((tile.type == TileType.ship1 || tile.type == TileType.ship2 || tile.type == TileType.ship3 || tile.type == TileType.ship4) & pirates[tempStep, 0].gold > 0)
                    {
                        pirates[tempStep, 0].gold = 0;
                        score[tempStep] += 1;
                        for (int i = 0; i < countOfPlayers; i++)
                        {
                            scoreLabels[i].Text = $"{i + 1} команда: {score[i]}";
                        }
                    }
                    
                    pirates[tempStep, 0].position = new Position(ny, nx);
                    tempStep += (1 - repeatMove);
                    tempStep = tempStep % countOfPlayers;
                    step.Text = $"текущий ход: {tempStep + 1}";
                    pirateWasClicked = false;
                    pirate1WasChosen = false;
                }

                else if (pirate2WasChosen & Math.Abs(ny - oy) <= 1 & Math.Abs(nx - ox) <= 1 & pirates[tempStep, 1].alive)
                {
                    var repeatMove = 0;
                    var player = tempPlayer();
                    if (tiles[oy, ox].type == TileType.ship1 || tiles[oy, ox].type == TileType.ship2 ||
                            tiles[oy, ox].type == TileType.ship3 || tiles[oy, ox].type == TileType.ship4)
                    {
                        if (tiles[oy, ox].type == TileType.ship1)
                        {
                            if (ny != oy + 1 || nx != ox)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }

                        else if (tiles[oy, ox].type == TileType.ship2)
                        {
                            if (ny != oy - 1 || nx != ox)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }

                        else if (tiles[oy, ox].type == TileType.ship3)
                        {
                            if (ny != oy || nx != ox + 1)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }

                        else if (tiles[oy, ox].type == TileType.ship4)
                        {
                            if (ny != oy || nx != ox - 1)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove=1;
                            }            
                        }
                    }
                    if (tile.type == TileType.ice)
                    {
                        tile.Open();
                        buttons[ny, nx].Image = tiles[ny, nx].image;
                        tiles[oy, ox].Pirate2 = false;
                        var y = ny - oy;
                        var x = nx - ox;
                        oy = ny;
                        ox = nx;
                        ny += y;
                        nx += x;
                        if (tiles[ny, nx].type == TileType.croc)
                        {
                            tiles[ny, nx].Open();
                            buttons[ny, nx].Image = tiles[ny, nx].image;
                            ny = oy - y;
                            nx = ox - x;
                            pirates[tempStep, 1].position = new Position(ny, nx);
                            tiles[ny, nx].Pirate2 = true;
                            tiles[ny, nx].putPirateOnTile(player);
                        }
                        else if (tiles[ny, nx].type == TileType.water)
                        {
                            pirates[tempStep, 1].alive = false;
                            pirates[tempStep, 1].gold = 0;
                            pirates[tempStep, 1].position = new Position(-1, -1);
                        }
                        else if (tiles[ny, nx].type == TileType.ship1 || tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship4)
                        {
                            if (tiles[ny, nx].team == Player.white & (tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship4))
                            {
                                pirates[tempStep, 1].alive = false;
                                pirates[tempStep, 1].gold = 0;
                                pirates[tempStep, 1].position = new Position(-1, -1);
                            }

                            else if (tiles[ny, nx].team == Player.black & (tiles[ny, nx].type == TileType.ship1 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship4))
                            {
                                pirates[tempStep, 1].alive = false;
                                pirates[tempStep, 1].gold = 0;
                                pirates[tempStep, 1].position = new Position(-1, -1);
                            }

                            else if (tiles[ny, nx].team == Player.red & (tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship1 || tiles[ny, nx].type == TileType.ship4))
                            {
                                pirates[tempStep, 1].alive = false;
                                pirates[tempStep, 1].gold = 0;
                                pirates[tempStep, 1].position = new Position(-1, -1);
                            }

                            else if (tiles[ny, nx].team == Player.yellow & (tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship1))
                            {
                                pirates[tempStep, 1].alive = false;
                                pirates[tempStep, 1].gold = 0;
                                pirates[tempStep, 1].position = new Position(-1, -1);
                            }
                        }

                        else
                        {
                            if (!tiles[oy - y, ox - x].Pirate1 & !tiles[oy - y, ox - x].Pirate2 & !tiles[oy - y, ox - x].Pirate3)
                            {
                                tiles[oy - y, ox - x].Open(removePirates: true);
                                buttons[oy - y, ox - x].Image = tiles[oy - y, ox - x].image;
                            }
                            Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                            repeatMove = 1;
                        }
                    }
                    else if (tile.team != Player.none & tile.team != tempPlayer() & pirates[tempStep, 1].gold == 0 & tile.type != TileType.fort)
                    {
                        tiles[oy, ox].Pirate2 = false;
                        if (tile.team == Player.white)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[0, i].type == TileType.ship1)
                                {
                                    y = 0;
                                    x = i;
                                }
                            }
                            tiles[y, x].team = Player.white;
                            tiles[y, x].putPirateOnTile(Player.white);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[0, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[0, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[0, 2].position = new Position(y, x);
                            }
                        }
                        else if (tile.team == Player.black)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[sizeOfField - 1, i].type == TileType.ship2)
                                {
                                    y = sizeOfField - 1;
                                    x = i;
                                }
                            }
                            tiles[y, x].team = Player.black;
                            tiles[y, x].putPirateOnTile(Player.black);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[1, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[1, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[1, 2].position = new Position(y, x);
                            }
                        }
                        else if (tile.team == Player.red)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[i, 0].type == TileType.ship3)
                                {
                                    y = i;
                                    x = 0;
                                }
                            }
                            tiles[y, x].team = Player.red;
                            tiles[y, x].putPirateOnTile(Player.red);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[2, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[2, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[2, 2].position = new Position(y, x);
                            }
                        }
                        else if (tile.team == Player.yellow)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[i, sizeOfField - 1].type == TileType.ship4)
                                {
                                    y = i;
                                    x = sizeOfField - 1;
                                }
                            }
                            tiles[y, x].team = Player.yellow;
                            tiles[y, x].putPirateOnTile(Player.yellow);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[3, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[3, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[3, 2].position = new Position(y, x);
                            }
                        }
                        tile.Pirate2 = true;
                        tile.team = tempPlayer();
                        tile.putPirateOnTile(tempPlayer());
                        pirates[tempStep, 1].position = new Position(ny, nx);
                        button.Image = tile.image;
                    }
                    else if (tile.team == Player.none || tile.team == tempPlayer())
                    {
                        tile.Open();
                        button.Image = tile.image;
                        if (tile.type == TileType.cannibal)
                        {
                            pirates[tempStep, 1].alive = false;
                            pirates[tempStep, 1].gold = 0;
                            pirates[tempStep, 1].position = new Position(-1, -1);
                            tiles[oy, ox].Pirate2 = false;
                            ny = -1;
                            nx = -1;
                        }
                        else if (tile.type == TileType.croc)
                        {
                            ny = oy;
                            nx = ox;
                        }
                        else if (tile.type == TileType.balloon)
                        {
                            if (player == Player.white)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[0, i].type == TileType.ship1)
                                    {
                                        y = 0;
                                        x = i;
                                    }
                                }
                                tiles[oy, ox].Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                            else if (player == Player.black)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[sizeOfField - 1, i].type == TileType.ship2)
                                    {
                                        y = sizeOfField - 1;
                                        x = i;
                                    }
                                }
                                tiles[oy, ox].Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                            else if (player == Player.red)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[i, 0].type == TileType.ship3)
                                    {
                                        y = i;
                                        x = 0;
                                    }
                                }
                                tiles[oy, ox].Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                            else if (player == Player.yellow)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[i, sizeOfField - 1].type == TileType.ship4)
                                    {
                                        y = i;
                                        x = sizeOfField - 1;
                                    }
                                }
                                tiles[oy, ox].Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                        }
                        else if (tile.type == TileType.gfort)
                        {
                            tile.putPirateOnTile(player);
                            tile.team = player;
                            tiles[oy, ox].Pirate2 = false;
                            tile.Pirate2 = true;
                            buttons[ny, nx].Image = tiles[ny, nx].image;
                            if (!pirates[tempStep, 0].alive)
                            {
                                pirates[tempStep, 0].alive = true;
                                pirates[tempStep, 0].position = new Position(ny, nx);
                                tile.Pirate1 = true;
                            }
                            if (!pirates[tempStep, 2].alive)
                            {
                                pirates[tempStep, 2].alive = true;
                                pirates[tempStep, 2].position = new Position(ny, nx);
                                tile.Pirate3 = true;
                            }
                        }
                        else
                        {
                            tile.putPirateOnTile(player);
                            tile.team = player;
                            tiles[oy, ox].Pirate2 = false;
                            tile.Pirate2 = true;
                            buttons[ny, nx].Image = tiles[ny, nx].image;
                        }
                    }
                    if (!tiles[oy, ox].Pirate1 & !tiles[oy, ox].Pirate2 & !tiles[oy, ox].Pirate3)
                    {
                        tiles[oy, ox].Open(removePirates: true);
                        buttons[oy, ox].Image = tiles[oy, ox].image;
                    }

                    if ((tile.type == TileType.ship1 || tile.type == TileType.ship2 || tile.type == TileType.ship3 || tile.type == TileType.ship4) & pirates[tempStep, 1].gold > 0)
                    {
                        pirates[tempStep, 1].gold = 0;
                        score[tempStep] += 1;
                        for (int i = 0; i < countOfPlayers; i++)
                        {
                            scoreLabels[i].Text = $"{i + 1} команда: {score[i]}";
                        }
                    }

                    pirates[tempStep, 1].position = new Position(ny, nx);
                    tempStep += (1 - repeatMove);
                    tempStep = tempStep % countOfPlayers;
                    step.Text = $"текущий ход: {tempStep + 1}";
                    pirateWasClicked = false;
                    pirate2WasChosen = false;
                }

                else if (pirate3WasChosen & Math.Abs(ny - oy) <= 1 & Math.Abs(nx - ox) <= 1 & pirates[tempStep, 2].alive)
                {
                    var player = tempPlayer();
                    var repeatMove = 0;
                    if (tiles[oy, ox].type == TileType.ship1 || tiles[oy, ox].type == TileType.ship2 ||
                            tiles[oy, ox].type == TileType.ship3 || tiles[oy, ox].type == TileType.ship4)
                    {
                        if (tiles[oy, ox].type == TileType.ship1)
                        {
                            if (ny != oy + 1 || nx != ox)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }

                        else if (tiles[oy, ox].type == TileType.ship2)
                        {
                            if (ny != oy - 1 || nx != ox)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }

                        else if (tiles[oy, ox].type == TileType.ship3)
                        {
                            if (ny != oy || nx != ox + 1)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }

                        else if (tiles[oy, ox].type == TileType.ship4)
                        {
                            if (ny != oy || nx != ox - 1)
                            {
                                ny = oy;
                                nx = ox;
                                tile = tiles[ny, nx];
                                button = buttons[ny, nx];
                                repeatMove = 1;
                            }
                        }
                    }
                    if (tile.type == TileType.ice)
                    {
                        tile.Open();
                        buttons[ny, nx].Image = tiles[ny, nx].image;
                        tiles[oy, ox].Pirate3 = false;
                        var y = ny - oy;
                        var x = nx - ox;
                        oy = ny;
                        ox = nx;
                        ny += y;
                        nx += x;
                        if (tiles[ny, nx].type == TileType.croc)
                        {
                            tiles[ny, nx].Open();
                            buttons[ny, nx].Image = tiles[ny, nx].image;
                            ny = oy - y;
                            nx = ox - x;
                            pirates[tempStep, 2].position = new Position(ny, nx);
                            tiles[ny, nx].Pirate3 = true;
                            tiles[ny, nx].putPirateOnTile(player);
                        }
                        else if (tiles[ny, nx].type == TileType.water)
                        {
                            pirates[tempStep, 2].alive = false;
                            pirates[tempStep, 2].gold = 0;
                            pirates[tempStep, 2].position = new Position(-1, -1);
                        }
                        else if (tiles[ny, nx].type == TileType.ship1 || tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship4)
                        {
                            if (tiles[ny, nx].team == Player.white & (tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship4))
                            {
                                pirates[tempStep, 2].alive = false;
                                pirates[tempStep, 2].gold = 0;
                                pirates[tempStep, 2].position = new Position(-1, -1);
                            }

                            else if (tiles[ny, nx].team == Player.black & (tiles[ny, nx].type == TileType.ship1 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship4))
                            {
                                pirates[tempStep, 2].alive = false;
                                pirates[tempStep, 2].gold = 0;
                                pirates[tempStep, 2].position = new Position(-1, -1);
                            }

                            else if (tiles[ny, nx].team == Player.red & (tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship1 || tiles[ny, nx].type == TileType.ship4))
                            {
                                pirates[tempStep, 2].alive = false;
                                pirates[tempStep, 2].gold = 0;
                                pirates[tempStep, 2].position = new Position(-1, -1);
                            }

                            else if (tiles[ny, nx].team == Player.yellow & (tiles[ny, nx].type == TileType.ship2 ||
                            tiles[ny, nx].type == TileType.ship3 || tiles[ny, nx].type == TileType.ship1))
                            {
                                pirates[tempStep, 2].alive = false;
                                pirates[tempStep, 2].gold = 0;
                                pirates[tempStep, 2].position = new Position(-1, -1);
                            }
                        }

                        else
                        {
                            if (!tiles[oy - y, ox - x].Pirate1 & !tiles[oy - y, ox - x].Pirate2 & !tiles[oy - y, ox - x].Pirate3)
                            {
                                tiles[oy - y, ox - x].Open(removePirates: true);
                                buttons[oy - y, ox - x].Image = tiles[oy - y, ox - x].image;
                            }
                            Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                            repeatMove = 1;
                        }
                    }
                    else if (tile.team != Player.none & tile.team != tempPlayer() & pirates[tempStep, 2].gold == 0 & tile.type != TileType.fort)
                    {
                        tiles[oy, ox].Pirate3 = false;
                        if (tile.team == Player.white)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[0, i].type == TileType.ship1)
                                {
                                    y = 0;
                                    x = i;
                                }
                            }
                            tiles[y, x].team = Player.white;
                            tiles[y, x].putPirateOnTile(Player.white);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[0, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[0, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[0, 2].position = new Position(y, x);
                            }
                        }
                        else if (tile.team == Player.black)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[sizeOfField - 1, i].type == TileType.ship2)
                                {
                                    y = sizeOfField - 1;
                                    x = i;
                                }
                            }
                            tiles[y, x].team = Player.black;
                            tiles[y, x].putPirateOnTile(Player.black);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[1, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[1, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[1, 2].position = new Position(y, x);
                            }
                        }
                        else if (tile.team == Player.red)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[i, 0].type == TileType.ship3)
                                {
                                    y = i;
                                    x = 0;
                                }
                            }
                            tiles[y, x].team = Player.red;
                            tiles[y, x].putPirateOnTile(Player.red);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[2, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[2, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[2, 2].position = new Position(y, x);
                            }
                        }
                        else if (tile.team == Player.yellow)
                        {
                            var y = -1;
                            var x = -1;
                            for (int i = 2; i < sizeOfField - 2; i++)
                            {
                                if (tiles[i, sizeOfField - 1].type == TileType.ship4)
                                {
                                    y = i;
                                    x = sizeOfField - 1;
                                }
                            }
                            tiles[y, x].team = Player.yellow;
                            tiles[y, x].putPirateOnTile(Player.yellow);
                            buttons[y, x].Image = tiles[y, x].image;

                            if (tile.Pirate1)
                            {
                                tile.Pirate1 = false;
                                tiles[y, x].Pirate1 = true;
                                pirates[3, 0].position = new Position(y, x);
                            }
                            else if (tile.Pirate2)
                            {
                                tile.Pirate2 = false;
                                tiles[y, x].Pirate2 = true;
                                pirates[3, 1].position = new Position(y, x);
                            }
                            else if (tile.Pirate3)
                            {
                                tile.Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                pirates[3, 2].position = new Position(y, x);
                            }
                        }
                        tile.Pirate3 = true;
                        tile.team = tempPlayer();
                        tile.putPirateOnTile(tempPlayer());
                        pirates[tempStep, 2].position = new Position(ny, nx);
                        button.Image = tile.image;
                    }
                    else if (tile.team == Player.none || tile.team == tempPlayer())
                    {
                        tile.Open();
                        button.Image = tile.image;
                        if (tile.type == TileType.cannibal)
                        {
                            pirates[tempStep, 2].alive = false;
                            pirates[tempStep, 2].gold = 0;
                            pirates[tempStep, 2].position = new Position(-1, -1);
                            tiles[oy, ox].Pirate3 = false;
                            ny = -1;
                            nx = -1;
                        }
                        else if (tile.type == TileType.croc)
                        {
                            ny = oy;
                            nx = ox;
                        }
                        else if (tile.type == TileType.balloon)
                        {
                            if (player == Player.white)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[0, i].type == TileType.ship1)
                                    {
                                        y = 0;
                                        x = i;
                                    }
                                }
                                tiles[oy, ox].Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                            else if (player == Player.black)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[sizeOfField - 1, i].type == TileType.ship2)
                                    {
                                        y = sizeOfField - 1;
                                        x = i;
                                    }
                                }
                                tiles[oy, ox].Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                            else if (player == Player.red)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[i, 0].type == TileType.ship3)
                                    {
                                        y = i;
                                        x = 0;
                                    }
                                }
                                tiles[oy, ox].Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                            else if (player == Player.yellow)
                            {
                                var y = -1;
                                var x = -1;
                                for (int i = 2; i < sizeOfField - 2; i++)
                                {
                                    if (tiles[i, sizeOfField - 1].type == TileType.ship4)
                                    {
                                        y = i;
                                        x = sizeOfField - 1;
                                    }
                                }
                                tiles[oy, ox].Pirate3 = false;
                                tiles[y, x].Pirate3 = true;
                                tiles[y, x].putPirateOnTile(player);
                                tiles[y, x].team = player;
                                buttons[y, x].Image = tiles[y, x].image;
                                ny = y;
                                nx = x;
                            }
                        }
                        else if (tile.type == TileType.gfort)
                        {
                            tile.putPirateOnTile(player);
                            tile.team = player;
                            tiles[oy, ox].Pirate3 = false;
                            tile.Pirate3 = true;
                            buttons[ny, nx].Image = tiles[ny, nx].image;
                            if (!pirates[tempStep, 1].alive)
                            {
                                pirates[tempStep, 1].alive = true;
                                pirates[tempStep, 1].position = new Position(ny, nx);
                                tile.Pirate2 = true;
                            }
                            if (!pirates[tempStep, 0].alive)
                            {
                                pirates[tempStep, 0].alive = true;
                                pirates[tempStep, 0].position = new Position(ny, nx);
                                tile.Pirate1 = true;
                            }
                        }
                        else
                        {
                            tile.putPirateOnTile(player);
                            tile.team = player;
                            tiles[oy, ox].Pirate3 = false;
                            tile.Pirate3 = true;
                            buttons[ny, nx].Image = tiles[ny, nx].image;
                        }
                    }

                    if (!tiles[oy, ox].Pirate1 & !tiles[oy, ox].Pirate2 & !tiles[oy, ox].Pirate3)
                    {
                        tiles[oy, ox].Open(removePirates: true);
                        buttons[oy, ox].Image = tiles[oy, ox].image;
                    }

                    if ((tile.type == TileType.ship1 || tile.type == TileType.ship2 || tile.type == TileType.ship3 || tile.type == TileType.ship4) & pirates[tempStep, 2].gold > 0)
                    {
                        pirates[tempStep, 2].gold = 0;
                        score[tempStep] += 1;
                        for (int i = 0; i < countOfPlayers; i++)
                        {
                            scoreLabels[i].Text = $"{i + 1} команда: {score[i]}";
                        }
                    }

                    pirates[tempStep, 2].position = new Position(ny, nx);
                    tempStep += (1 - repeatMove);
                    tempStep = tempStep % countOfPlayers;
                    step.Text = $"текущий ход: {tempStep + 1}";
                    pirateWasClicked = false;
                    pirate3WasChosen = false;
                }

                else
                {
                    message.Text = "Невозможно сделать ход";
                }

                button1.Text = $"пират 1 ({pirates[tempStep, 0].gold})";
                button2.Text = $"пират 2 ({pirates[tempStep, 1].gold})";
                button3.Text = $"пират 3 ({pirates[tempStep, 2].gold})";
            }
            else
            {
                message.Text = "Выбрана старая позиция";
            }
            if (score[0] + score[1] + score[2] + score[3] == countOfCoins)
            {
                Win();
            }
            var deads = 0;
            for (int i = 0; i < countOfPlayers; i++)
            {
                if (pirates[i, 0].alive == false & pirates[i, 1].alive == false & pirates[i, 2].alive == false)
                {
                    score[i] = -1;
                    deads += 1;
                }
                if (countOfPlayers < 4)
                {
                    score[3] = -2;
                }
                if (countOfPlayers < 3)
                {
                    score[2] = -2;
                }
            }
            if (countOfPlayers - deads <= 1)
            {
                Win();
            }
        }

        public void MoveShip(Button button, int ny, int nx, Tile tile)
        {
            if (nx != ox || ny != oy)
            {
                if (tempStep == 0)
                {
                    if ((nx - ox) <= 1 & ny == oy & nx > 1 & nx < sizeOfField - 2)
                    {
                        tile.SetShip(tempStep);
                        tiles[oy, ox].SetWater();
                        button.Image = tile.image;
                        tile.team = Player.white;
                        tiles[oy, ox].team = Player.none;
                        buttons[oy, ox].Image = tiles[oy, ox].image;
                        if (tiles[oy, ox].Pirate1)
                        {
                            tile.Pirate1 = true;
                            tiles[oy, ox].Pirate1 = false;
                            tile.putPirateOnTile(Player.white);
                            button.Image = tile.image;
                        }
                        if (tiles[oy, ox].Pirate2)
                        {
                            tile.Pirate2 = true;
                            tiles[oy, ox].Pirate2 = false;
                            tile.putPirateOnTile(Player.white);
                            button.Image = tile.image;
                        }
                        if (tiles[oy, ox].Pirate3)
                        {
                            tile.Pirate3 = true;
                            tiles[oy, ox].Pirate3 = false;
                            tile.putPirateOnTile(Player.white);
                            button.Image = tile.image;
                        }
                        if (!tiles[oy, ox].Pirate1 & !tiles[oy, ox].Pirate2 & !tiles[oy, ox].Pirate3)
                        {
                            tiles[oy, ox].Open(removePirates: true);
                            buttons[oy, ox].BackgroundImage = tiles[oy, ox].image;
                        }
                        tempStep += 1;
                        tempStep = tempStep % countOfPlayers;
                        step.Text = $"текущий ход: {tempStep + 1}";
                        shipWasChosen = false;
                    }
                }

                else if (tempStep == 1)
                {
                    if ((nx - ox) <= 1 & ny == oy & nx > 1 & nx < sizeOfField - 2)
                    {
                        tile.SetShip(tempStep);
                        tiles[oy, ox].SetWater();
                        tile.team = Player.black;
                        tiles[oy, ox].team = Player.none;
                        button.Image = tile.image;
                        buttons[oy, ox].Image = tiles[oy, ox].image;
                        if (tiles[oy, ox].Pirate1)
                        {
                            tile.Pirate1 = true;
                            tiles[oy, ox].Pirate1 = false;
                            pirates[tempStep, 0].position = new Position(oy, ox);
                            tile.putPirateOnTile(Player.black);
                            button.Image = tile.image;
                        }
                        if (tiles[oy, ox].Pirate2)
                        {
                            tile.Pirate2 = true;
                            tiles[oy, ox].Pirate2 = false;
                            pirates[tempStep, 1].position = new Position(oy, ox);
                            tile.putPirateOnTile(Player.black);
                            button.Image = tile.image;
                        }
                        if (tiles[oy, ox].Pirate3)
                        {
                            tile.Pirate3 = true;
                            tiles[oy, ox].Pirate3 = false;
                            pirates[tempStep, 2].position = new Position(oy, ox);
                            tile.putPirateOnTile(Player.black);
                            button.Image = tile.image;
                        }
                        if (!tiles[oy, ox].Pirate1 & !tiles[oy, ox].Pirate2 & !tiles[oy, ox].Pirate3)
                        {
                            tiles[oy, ox].Open(removePirates: true);
                            buttons[oy, ox].BackgroundImage = tiles[oy, ox].image;
                        }
                        tempStep += 1;
                        tempStep = tempStep % countOfPlayers;
                        step.Text = $"текущий ход: {tempStep + 1}";
                        shipWasChosen = false;
                    }
                }

                else if (tempStep == 2)
                {
                    if ((ny - oy) <= 1 & nx == ox & ny > 1 & ny < sizeOfField - 2)
                    {
                        tile.SetShip(tempStep);
                        tiles[oy, ox].SetWater();
                        tile.team = Player.red;
                        tiles[oy, ox].team = Player.none;
                        button.Image = tile.image;
                        buttons[oy, ox].Image = tiles[oy, ox].image;
                        if (tiles[oy, ox].Pirate1)
                        {
                            tile.Pirate1 = true;
                            tiles[oy, ox].Pirate1 = false;
                            pirates[tempStep, 0].position = new Position(oy, ox);
                            tile.putPirateOnTile(Player.red);
                            button.Image = tile.image;
                        }
                        if (tiles[oy, ox].Pirate2)
                        {
                            tile.Pirate2 = true;
                            tiles[oy, ox].Pirate2 = false;
                            pirates[tempStep, 1].position = new Position(oy, ox);
                            tile.putPirateOnTile(Player.red);
                            button.Image = tile.image;
                        }
                        if (tiles[oy, ox].Pirate3)
                        {
                            tile.Pirate3 = true;
                            tiles[oy, ox].Pirate3 = false;
                            pirates[tempStep, 2].position = new Position(oy, ox);
                            tile.putPirateOnTile(Player.red);
                            button.Image = tile.image;
                        }
                        if (!tiles[oy, ox].Pirate1 & !tiles[oy, ox].Pirate2 & !tiles[oy, ox].Pirate3)
                        {
                            tiles[oy, ox].Open(removePirates: true);
                            buttons[oy, ox].BackgroundImage = tiles[oy, ox].image;
                        }
                        tempStep += 1;
                        tempStep = tempStep % countOfPlayers;
                        step.Text = $"текущий ход: {tempStep + 1}";
                        shipWasChosen = false;
                    }
                }

                else if (tempStep == 3)
                {
                    if ((ny - oy) <= 1 & nx == ox & ny > 1 & ny < sizeOfField - 2)
                    {
                        tile.SetShip(tempStep);
                        tiles[oy, ox].SetWater();
                        tile.team = Player.yellow;
                        tiles[oy, ox].team = Player.none;
                        button.Image = tile.image;
                        buttons[oy, ox].Image = tiles[oy, ox].image;
                        if (tiles[oy, ox].Pirate1)
                        {
                            tile.Pirate1 = true;
                            tiles[oy, ox].Pirate1 = false;
                            pirates[tempStep, 0].position = new Position(oy, ox);
                            tile.putPirateOnTile(Player.yellow);
                            button.Image = tile.image;
                        }
                        if (tiles[oy, ox].Pirate2)
                        {
                            tile.Pirate2 = true;
                            tiles[oy, ox].Pirate2 = false;
                            pirates[tempStep, 1].position = new Position(oy, ox);
                            tile.putPirateOnTile(Player.yellow);
                            button.Image = tile.image;
                        }
                        if (tiles[oy, ox].Pirate3)
                        {
                            tile.Pirate3 = true;
                            tiles[oy, ox].Pirate3 = false;
                            pirates[tempStep, 2].position = new Position(oy, ox);
                            tile.putPirateOnTile(Player.yellow);
                            button.Image = tile.image;
                        }
                        if (!tiles[oy, ox].Pirate1 & !tiles[oy, ox].Pirate2 & !tiles[oy, ox].Pirate3)
                        {
                            tiles[oy, ox].Open(removePirates: true);
                            buttons[oy, ox].BackgroundImage = tiles[oy, ox].image;
                        }
                        tempStep += 1;
                        tempStep = tempStep % countOfPlayers;
                        step.Text = $"текущий ход: {tempStep + 1}";
                        shipWasChosen = false;
                    }
                }

                button1.Text = $"пират 1 ({pirates[tempStep, 0].gold})";
                button2.Text = $"пират 2 ({pirates[tempStep, 1].gold})";
                button3.Text = $"пират 3 ({pirates[tempStep, 2].gold})";
            }
            else
            {
                message.Text = "Выбрана старая позиция";
            }
        }

        public void clickOnPirateButton1(object sender, EventArgs e)
        {
            Player teamType = Player.none;
            pirateWasClicked = true;
            pirate1WasChosen = true;
            pirate2WasChosen = false;
            pirate3WasChosen = false;

            if (tempStep == 0)
            {
                teamType = Player.white;
            }

            else if (tempStep == 1)
            {
                teamType = Player.black;
            }

            else if (tempStep == 2)
            {
                teamType = Player.red;
            }

            else if (tempStep == 3)
            {
                teamType = Player.yellow;
            }


            for (int i = 0; i < sizeOfField; i++)
            {

                for (int j = 0; j < sizeOfField; j++)
                {
                    if (tiles[i, j].team == teamType && tiles[i, j].Pirate1)
                    {
                        ox = j;
                        oy = i;
                    }
                }
            }
            message.Text = $"{oy}, {ox}";
        }

        public void clickOnPirateButton2(object sender, EventArgs e)
        {
            Player teamType = Player.none;
            pirateWasClicked = true;
            pirate1WasChosen = false;
            pirate3WasChosen = false;
            pirate2WasChosen = true;

            if (tempStep == 0)
            {
                teamType = Player.white;
            }

            else if (tempStep == 1)
            {
                teamType = Player.black;
            }

            else if (tempStep == 2)
            {
                teamType = Player.red;
            }

            else if (tempStep == 3)
            {
                teamType = Player.yellow;
            }


            for (int i = 0; i < sizeOfField; i++)
            {

                for (int j = 0; j < sizeOfField; j++)
                {
                    if (tiles[i, j].team == teamType && tiles[i, j].Pirate2)
                    {
                        ox = j;
                        oy = i;
                    }
                }
            }
            message.Text = $"{oy}, {ox}";
        }

        public void clickOnPirateButton3(object sender, EventArgs e)
        {
            Player teamType = Player.none;
            pirateWasClicked = true;
            pirate1WasChosen = false;
            pirate2WasChosen = false;
            pirate3WasChosen = true;

            if (tempStep == 0)
            {
                teamType = Player.white;
            }

            else if (tempStep == 1)
            {
                teamType = Player.black;
            }

            else if (tempStep == 2)
            {
                teamType = Player.red;
            }

            else if (tempStep == 3)
            {
                teamType = Player.yellow;
            }


            for (int i = 0; i < sizeOfField; i++)
            {

                for (int j = 0; j < sizeOfField; j++)
                {
                    if (tiles[i, j].team == teamType && tiles[i, j].Pirate3)
                    {
                        ox = j;
                        oy = i;
                    }
                }
            }
            message.Text = $"{oy}, {ox}";
        }

        public void clickOnShip(object sender, EventArgs e)
        {
            if (tempStep == 0)
            {
                for (int i = 2; i < sizeOfField - 2; i++)
                {
                    if (tiles[0, i].type == TileType.ship1)
                    {
                        shipWasChosen = true;
                        oy = 0;
                        ox = i;
                    }
                }
            }

            else if (tempStep == 1)
            {
                for (int i = 2; i < sizeOfField - 2; i++)
                {
                    if (tiles[sizeOfField - 1, i].type == TileType.ship2)
                    {
                        shipWasChosen = true;
                        oy = sizeOfField - 1;
                        ox = i;
                    }
                }
            }

            else if (tempStep == 2)
            {
                for (int i = 2; i < sizeOfField - 2; i++)
                {
                    if (tiles[i, 0].type == TileType.ship3)
                    {
                        shipWasChosen = true;
                        oy = i;
                        ox = 0;
                    }
                }
            }

            else if (tempStep == 3)
            {
                for (int i = 2; i < sizeOfField - 2; i++)
                {
                    if (tiles[i, sizeOfField - 1].type == TileType.ship4)
                    {
                        shipWasChosen = true;
                        oy = i;
                        ox = sizeOfField - 1;
                    }
                }
            }
        }

        public void TakeCoin(object sender, EventArgs e)
        {
            if (pirate1WasChosen & pirates[tempStep, 0].gold == 0)
            {
                var pos = pirates[tempStep, 0].position;
                var tile = tiles[pos.x, pos.y];
                if (tile.coins > 0)
                {
                    pirates[tempStep, 0].gold = 1;
                    tile.coins -= 1;
                }
                pirateWasClicked = false;
                pirate1WasChosen = false;
                tempStep += 1;
                tempStep = tempStep % countOfPlayers;
            }

            else if (pirate2WasChosen & pirates[tempStep, 1].gold == 0)
            {
                var pos = pirates[tempStep, 1].position;
                var tile = tiles[pos.x, pos.y];
                if (tile.coins > 0)
                {
                    pirates[tempStep, 1].gold = 1;
                    tile.coins -= 1;
                }
                pirateWasClicked = false;
                pirate2WasChosen = false;
                tempStep += 1;
                tempStep = tempStep % countOfPlayers;
            }

            else if (pirate3WasChosen & pirates[tempStep, 2].gold == 0)
            {
                var pos = pirates[tempStep, 2].position;
                var tile = tiles[pos.x, pos.y];
                if (tile.coins > 0)
                {
                    pirates[tempStep, 2].gold = 1;
                    tile.coins -= 1;
                }
                pirateWasClicked = false;
                pirate3WasChosen = false;
                tempStep += 1;
                tempStep = tempStep % countOfPlayers;
            }
            step.Text = $"текущий ход: {tempStep + 1}";
            button1.Text = $"пират 1 ({pirates[tempStep, 0].gold})";
            button2.Text = $"пират 2 ({pirates[tempStep, 1].gold})";
            button3.Text = $"пират 3 ({pirates[tempStep, 2].gold})";
        }

        public void deselect(object sender, EventArgs e)
        {
            pirateWasClicked = false;
            pirate1WasChosen = false;
            pirate2WasChosen = false;
            pirate3WasChosen = false;
            shipWasChosen = false;
            ox = 0;
            oy = 0;
            stop = true;
        }

        public void nextBotMove(object sender, EventArgs e)
        {
            if (typeOfPlayer[tempStep] == 1)
            {
                if (tempStep == 0)
                {
                    Bot1Move();
                    while (tempStep == 0)
                    {
                        Bot1Move();
                    }
                }
                else if (tempStep == 1)
                {
                    Bot2Move();
                    while (tempStep == 1)
                    {
                        Bot2Move();
                    }
                }
                else if (tempStep == 2)
                {
                    Bot3Move();
                    while (tempStep == 2)
                    {
                        Bot3Move();
                    }
                }
                else if (tempStep == 3)
                {
                    Bot4Move();
                    while (tempStep == 3)
                    {
                        Bot4Move();
                    }
                }
            }
            step.Text = $"текущий ход: {tempStep + 1}";
            button1.Text = $"пират 1 ({pirates[tempStep, 0].gold})";
            button2.Text = $"пират 2 ({pirates[tempStep, 1].gold})";
            button3.Text = $"пират 3 ({pirates[tempStep, 2].gold})";
        }

        public void clickOnTile(object sender, EventArgs e)
        {
            var button = sender as Button;
            var pos = button.Location;
            var ny = pos.Y / sizeOfCell;
            var nx = pos.X / sizeOfCell;
            var tile = tiles[ny, nx];

            if (tile.opened == false)
            {
                message.Text = "closed";
            }
            else
            {
                if (tile.coins > 0)
                {
                    message.Text = $"{tile.type}, ({tile.coins})";
                }
                else
                {
                    message.Text = tile.type.ToString();
                }
            }

            if (shipWasChosen)
            {
                MoveShip(button, ny, nx, tile);
            }

            else if (pirateWasClicked)
            {
                Move(button, ny, nx, tile);
            }

        }

        public Player tempPlayer()
        {
            if (tempStep == 0)
            {
                return Player.white;
            }

            else if (tempStep == 1)
            {
                return Player.black;
            }

            else if (tempStep == 2)
            {
                return Player.red;
            }

            else if (tempStep == 3)
            {
                return Player.yellow;
            }

            return Player.none;
        }
        public void Win()
        {
            Form5 form5 = new Form5(score[0], score[1], score[2], score[3], countOfPlayers);
            form5.Show();
            form.Hide();
        }
        public void Bot1Move()
        {
            if (bot1step == 0)
            {
                pirateWasClicked = true;
                pirate1WasChosen = true;
                pirate2WasChosen = false;
                pirate3WasChosen = false;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.white && tiles[i, j].Pirate1)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship1)
                {
                    Move(buttons[oy + 1, ox], oy + 1, ox, tiles[oy + 1, ox]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 0].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 0].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate1WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 0].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (ox > 6)
                    {
                        nx = ox - 1;
                    }
                    else if (ox < 6)
                    {
                        nx = ox + 1;
                    }
                    if (oy > 0)
                    {
                        ny = oy - 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        y = 1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            else if (bot1step == 1)
            {
                pirateWasClicked = true;
                pirate2WasChosen = true;
                pirate1WasChosen = false;
                pirate3WasChosen = false;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.white && tiles[i, j].Pirate2)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship1)
                {
                    Move(buttons[oy + 1, ox], oy + 1, ox, tiles[oy + 1, ox]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 1].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 1].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate2WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 1].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (ox > 6)
                    {
                        nx = ox - 1;
                    }
                    else if (ox < 6)
                    {
                        nx = ox + 1;
                    }
                    if (oy > 0)
                    {
                        ny = oy - 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        y = 1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
        
            else if (bot1step == 2)
            {
                pirateWasClicked = true;
                pirate1WasChosen = false;
                pirate2WasChosen = false;
                pirate3WasChosen = true;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.white && tiles[i, j].Pirate3)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship1)
                {
                    Move(buttons[oy + 1, ox], oy + 1, ox, tiles[oy + 1, ox]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 2].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 2].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate3WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 2].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (ox > 6)
                    {
                        nx = ox - 1;
                    }
                    else if (ox < 6)
                    {
                        nx = ox + 1;
                    }
                    if (oy > 0)
                    {
                        ny = oy - 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        y = 1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            bot1step += 1;
            bot1step = bot1step % 3;
        }
        public void Bot2Move()
        {
            if (bot2step == 0)
            {
                pirateWasClicked = true;
                pirate1WasChosen = true;
                pirate2WasChosen = false;
                pirate3WasChosen = false;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.black && tiles[i, j].Pirate1)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship2)
                {
                    Move(buttons[oy - 1, ox], oy - 1, ox, tiles[oy - 1, ox]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 0].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 0].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate1WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 0].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (ox > 6)
                    {
                        nx = ox - 1;
                    }
                    else if (ox < 6)
                    {
                        nx = ox + 1;
                    }
                    if (oy > 0)
                    {
                        ny = oy - 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        y = -1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            else if (bot2step == 1)
            {
                pirateWasClicked = true;
                pirate2WasChosen = true;
                pirate1WasChosen = false;
                pirate3WasChosen = false;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.black && tiles[i, j].Pirate2)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship2)
                {
                    Move(buttons[oy - 1, ox], oy - 1, ox, tiles[oy - 1, ox]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 1].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 1].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate1WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 1].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (ox > 6)
                    {
                        nx = ox - 1;
                    }
                    else if (ox < 6)
                    {
                        nx = ox + 1;
                    }
                    if (oy > 0)
                    {
                        ny = oy - 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        y = -1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            else if (bot2step == 2)
            {
                pirateWasClicked = true;
                pirate1WasChosen = false;
                pirate2WasChosen = false;
                pirate3WasChosen = true;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.black && tiles[i, j].Pirate3)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship2)
                {
                    Move(buttons[oy - 1, ox], oy - 1, ox, tiles[oy - 1, ox]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 2].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 2].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate1WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 2].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (ox > 6)
                    {
                        nx = ox - 1;
                    }
                    else if (ox < 6)
                    {
                        nx = ox + 1;
                    }
                    if (oy > 0)
                    {
                        ny = oy - 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        y = -1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            bot2step += 1;
            bot2step = bot2step % 3;
        }
        public void Bot3Move()
        {
            if (bot3step == 0)
            {
                pirateWasClicked = true;
                pirate1WasChosen = true;
                pirate2WasChosen = false;
                pirate3WasChosen = false;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.red && tiles[i, j].Pirate1)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship3)
                {
                    Move(buttons[oy, ox + 1], oy, ox + 1, tiles[oy, ox + 1]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 0].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 0].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate1WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 0].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (oy > 6)
                    {
                        ny = oy - 1;
                    }
                    else if (oy < 6)
                    {
                        ny = oy + 1;
                    }
                    if (ox > 0)
                    {
                        nx = ox - 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        x = 1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            if (bot3step == 1)
            {
                pirateWasClicked = true;
                pirate2WasChosen = true;
                pirate1WasChosen = false;
                pirate3WasChosen = false;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.red && tiles[i, j].Pirate2)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship3)
                {
                    Move(buttons[oy, ox + 1], oy, ox + 1, tiles[oy, ox + 1]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 1].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 1].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate2WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 1].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (oy > 6)
                    {
                        ny = oy - 1;
                    }
                    else if (oy < 6)
                    {
                        ny = oy + 1;
                    }
                    if (ox > 0)
                    {
                        nx = ox - 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        x = 1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            if (bot3step == 2)
            {
                pirateWasClicked = true;
                pirate1WasChosen = false;
                pirate2WasChosen = false;
                pirate3WasChosen = true;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.red && tiles[i, j].Pirate3)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship3)
                {
                    Move(buttons[oy, ox + 1], oy, ox + 1, tiles[oy, ox + 1]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 2].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 2].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate3WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 2].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (oy > 6)
                    {
                        ny = oy - 1;
                    }
                    else if (oy < 6)
                    {
                        ny = oy + 1;
                    }
                    if (ox > 0)
                    {
                        nx = ox - 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        x = 1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            bot3step += 1;
            bot3step = bot3step % 3;
        }
        public void Bot4Move()
        {
            if (bot4step == 0)
            {
                pirateWasClicked = true;
                pirate1WasChosen = true;
                pirate2WasChosen = false;
                pirate3WasChosen = false;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.yellow && tiles[i, j].Pirate1)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship4)
                {
                    Move(buttons[oy, ox - 1], oy, ox - 1, tiles[oy, ox - 1]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 0].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 0].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate1WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 0].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (oy > 6)
                    {
                        ny = oy - 1;
                    }
                    else if (oy < 6)
                    {
                        ny = oy + 1;
                    }
                    if (ox < sizeOfField - 1)
                    {
                        nx = ox + 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        x = -1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            if (bot4step == 1)
            {
                pirateWasClicked = true;
                pirate2WasChosen = true;
                pirate1WasChosen = false;
                pirate3WasChosen = false;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.yellow && tiles[i, j].Pirate2)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship4)
                {
                    Move(buttons[oy, ox - 1], oy, ox - 1, tiles[oy, ox - 1]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 1].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 1].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate2WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 1].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (oy > 6)
                    {
                        ny = oy - 1;
                    }
                    else if (oy < 6)
                    {
                        ny = oy + 1;
                    }
                    if (ox < sizeOfField - 1)
                    {
                        nx = ox + 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        x = -1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            if (bot4step == 2)
            {
                pirateWasClicked = true;
                pirate1WasChosen = false;
                pirate2WasChosen = false;
                pirate3WasChosen = true;

                for (int i = 0; i < sizeOfField; i++)
                {

                    for (int j = 0; j < sizeOfField; j++)
                    {
                        if (tiles[i, j].team == Player.yellow && tiles[i, j].Pirate3)
                        {
                            ox = j;
                            oy = i;
                        }
                    }
                }
                if (tiles[oy, ox].type == TileType.ship4)
                {
                    Move(buttons[oy, ox - 1], oy, ox - 1, tiles[oy, ox - 1]);
                }
                else if (tiles[oy, ox].coins > 0)
                {
                    var pos = pirates[tempStep, 2].position;
                    var tile = tiles[pos.x, pos.y];
                    if (tile.coins > 0)
                    {
                        pirates[tempStep, 2].gold = 1;
                        tile.coins -= 1;
                    }
                    pirateWasClicked = false;
                    pirate3WasChosen = false;
                    tempStep += 1;
                    tempStep = tempStep % countOfPlayers;
                }
                else if (pirates[tempStep, 2].gold > 0)
                {
                    int nx = ox;
                    int ny = oy;
                    if (oy > 6)
                    {
                        ny = oy - 1;
                    }
                    else if (oy < 6)
                    {
                        ny = oy + 1;
                    }
                    if (ox < sizeOfField - 1)
                    {
                        nx = ox + 1;
                    }
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
                else
                {
                    int y = rnd.Next(-1, 2);
                    int x = rnd.Next(-1, 2);
                    if (x == 0 & y == 0)
                    {
                        x = -1;
                    }
                    var ny = oy + y;
                    var nx = ox + x;
                    Move(buttons[ny, nx], ny, nx, tiles[ny, nx]);
                }
            }
            bot4step += 1;
            bot4step = bot4step % 3;
        }
    }
}
