﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jackal
{
    public partial class Form4 : Form
    {
        int countOfPlayers;
        Dictionary<int, int> typeOfPlayer;

        public Form4(int countOfPlayers, Dictionary<int, int> typeOfPlayer)
        {
            InitializeComponent();
            this.countOfPlayers = countOfPlayers;
            this.typeOfPlayer = typeOfPlayer;
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            var mapGenerator = new MapGenerator(countOfPlayers, this);
            var game = new Game(mapGenerator.buttons, mapGenerator.tiles, countOfPlayers, this, typeOfPlayer);

        }

        private void Form4_Closed(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonClick(object sender, EventArgs e)
        {

        }
    }
}
